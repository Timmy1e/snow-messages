# Snow Messages

## Synopsis
A tool to copy, download, and upload messages for translation on ServiceNow instances. 


## Installation
Requirements:
 - Go _(1.10.3+)_

Dependencies:
 - [go-input](https://github.com/tcnksm/go-input)
 - [cli](https://github.com/urfave/cli)
 
Installation command:
```bash
$ go get gitlab.com/Timmy1e/snow-messages
```


## Usage
The instance name is the part of the instance URL before the `.service-now.com`.
So the instance name of `https://myinstance.service-now.com/` is `myinstance`.
Global help:
```text
NAME:
   snowMessages - Copy/download/upload messages for translation on ServiceNow instances.

USAGE:
   snowMessages [global options] command [command options] [arguments...]

VERSION:
   0.1.0

COMMANDS:
     copy      Copy messages from one ServiceNow instance to another.
     download  Download messages from ServiceNow
     upload    Upload message to ServiceNow.
     help, h   Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

### Copy
To copy messages between ServiceNow instances.
You can override the language, and/or application name using parameters.
Copy help:
```text
NAME:
   snowMessages copy - Copy messages from one ServiceNow instance to another.

USAGE:
   snowMessages copy [command options] [SOURCE_INSTANCE_NAME] [DESTINATION_INSTANCE_NAME]

OPTIONS:
   --download-query ENCODED_QUERY, -q ENCODED_QUERY            Download the messages with ENCODED_QUERY.
   --upload-language LANG_CODE, -l LANG_CODE                   Upload the messages, force the language to LANG_CODE.
   --upload-application APPLICATION_NAME, -a APPLICATION_NAME  Upload the messages, force the application to APPLICATION_NAME.
```
Example:
```bash
$ snowMessages copy sourceInstanceName destinationInstanceName -q application=YourOldApplication -l en -a YourNewApplication
```

### Download
To download messages from a ServiceNow instance, NOT IMPLEMENTED.
Download help:
```text
NAME:
   snowMessages download - Download messages from ServiceNow

USAGE:
   snowMessages download [command options] [INSTANCE_NAME]

OPTIONS:
   --download-query ENCODED_QUERY, -q ENCODED_QUERY  Download the messages with ENCODED_QUERY.
```
Example:
```bash
$ snowMessages download instanceName -q application=YourApplication
```

### Upload
To upload messages to a ServiceNow instance, NOT IMPLEMENTED.
Upload help:
```text
NAME:
   snowMessages upload - Upload message to ServiceNow.

USAGE:
   snowMessages upload [command options] [INSTANCE_NAME]

OPTIONS:
   --upload-language LANG_CODE, -l LANG_CODE                   Upload the messages, force the language to LANG_CODE.
   --upload-application APPLICATION_NAME, -a APPLICATION_NAME  Upload the messages, force the application to APPLICATION_NAME.
```
Example:
```bash
$ snowMessages upload instanceName -l en -a YourApplication
```

## License
GNU General Public License v3.0
