/*
 * © 2018 ArchBox.xyz.
 * Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

package main

import (
	"net/http"
	"fmt"
	"os"
	"encoding/base64"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"github.com/tcnksm/go-input"
	"github.com/urfave/cli"
	"io"
)

var (
	client = &http.Client{}

	ui = &input.UI{
		Writer: os.Stdout,
		Reader: os.Stdin,
	}

	inputOptionDefault = input.Options{
		Required:  true,
		Loop:      true,
		HideOrder: true,
		Mask:      false,
	}

	inputOptionPassword = input.Options{
		Required:  true,
		Loop:      true,
		HideOrder: true,
		Mask:      true,
	}
)

/**
 * Function prints error and exits if the error is set.
 */
func panicOnError(description string, err error, exitCode int) {
	if err != nil {
		fmt.Printf("%s: %s", description, err)
		os.Exit(exitCode)
	}
}

/**
 * Convert username and password to basic auth string.
 */
func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

/**
 * Ask user for credentials, return an auth string.
 */
func AskCredentials(instance string) string {
	// Print instance url.
	fmt.Printf("%s.service-now.com:\n", instance)

	username, err := ui.Ask("  Username", &inputOptionDefault)
	panicOnError("Failed to get source username", err, 11)

	password, err := ui.Ask("\033[F  Password", &inputOptionPassword)
	panicOnError("Failed to get source password", err, 12)

	fmt.Print("\033[F")
	auth := "Basic " + basicAuth(username, password)
	return auth
}

/**
 * Main post request.
 */
func MakeGetRequest(instance string, authString string, query string) []Message {
	fmt.Print("Getting data... ")

	// Build url

	queryParam := ""
	if query != "" {
		queryParam = "&sysparm_query=" + query
	}

	url := fmt.Sprintf("https://%s.service-now.com/api/now/table/sys_ui_message?sysparm_fields=key,message,language,application%s", instance, queryParam)

	// Build request
	getRequest, err := http.NewRequest("GET", url, nil)
	panicOnError("Error creating GET getRequest", err, 11)

	// Add auth header
	getRequest.Header.Add("Authorization", authString)

	// Make request.
	getResponse, err := client.Do(getRequest)
	panicOnError("Error getting data from Service-Now", err, 12)
	fmt.Println("done.")

	// Parse messages, has to be done before closing.
	messages := ParseMessages(getResponse.Body)

	// Wait for request to finish.
	defer getResponse.Body.Close()

	return messages
}

/**
 * Parse response body to list of messages.
 */
func ParseMessages(body io.ReadCloser) []Message {
	fmt.Print("Parsing data... ")
	var snowResponse SnowResponse

	err := json.NewDecoder(body).Decode(&snowResponse)
	panicOnError("Parsing error", err, 31)
	fmt.Println("done.")

	return snowResponse.Result
}

/**
 * Post messages.
 */
func PostMessages(messages []Message, instance string, authString string, overrideMessage Message) {
	fmt.Print("Posting data... ")
	channel := make(chan []byte)
	url := fmt.Sprintf("https://%s.service-now.com/api/now/table/sys_ui_message", instance)

	for _, message := range messages {
		if overrideMessage.Application != "" {
			message.Application = overrideMessage.Application
		}
		if overrideMessage.Language != "" {
			message.Language = overrideMessage.Language
		}

		go MakePostRequest(authString, url, message, channel)
	}

	for range messages {
		if <-channel == nil {
			fmt.Print("Something went wrong! ")
		}
	}

	fmt.Println("done.")
}

/**
 * Main post request.
 */
func MakePostRequest(authString string, url string, message Message, ch chan<- []byte) {
	jsonData, err := json.Marshal(message)
	panicOnError("JSON conversion error", err, 41)

	postRequest, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	panicOnError("Error creating POST getRequest", err, 42)

	postRequest.Header.Set("Authorization", authString)
	postRequest.Header.Set("Content-Type", "application/json")
	postResponse, err := client.Do(postRequest)
	panicOnError("Error getting posting from Service-Now", err, 43)

	result, err := ioutil.ReadAll(postResponse.Body)
	panicOnError("Read error", err, 44)

	defer postResponse.Body.Close()
	ch <- result
}

func main() {
	var downloadQuery string
	var uploadMessage Message

	app := cli.NewApp()
	app.Name = "snowMessages"
	app.Usage = "Copy/download/upload messages for translation on ServiceNow instances."
	app.Version = "0.1.1"
	app.Copyright = "© 2019 Kiririn.io"

	app.Commands = []*cli.Command{
		{
			Name:      "copy",
			Usage:     "Copy messages from one ServiceNow instance to another.",
			ArgsUsage: "[SOURCE_INSTANCE_NAME] [DESTINATION_INSTANCE_NAME]",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "download-query, q",
					Usage:       "Download the messages with `ENCODED_QUERY`.",
					Destination: &downloadQuery,
				},
				&cli.StringFlag{
					Name:        "upload-language, l",
					Usage:       "Upload the messages, force the language to `LANG_CODE`.",
					Destination: &uploadMessage.Language,
				},
				&cli.StringFlag{
					Name:        "upload-application, a",
					Usage:       "Upload the messages, force the application to `APPLICATION_NAME`.",
					Destination: &uploadMessage.Application,
				},
			},
			Action: func(context *cli.Context) error {
				if context.Args().Len() != 2 {
					return cli.NewExitError("Requires 2 arguments, `SOURCE_INSTANCE_NAME` and `DESTINATION_INSTANCE_NAME`!", 1)
				}
				args := context.Args()
				sourceInstance := args.Get(0)
				destinationInstance := args.Get(1)

				// Source credentials.
				sourceAuthString := AskCredentials(sourceInstance)

				// Destination credentials.
				destinationAuthString := AskCredentials(destinationInstance)

				// Get messages from source instance, and parse messages.
				messages := MakeGetRequest(sourceInstance, sourceAuthString, downloadQuery)

				// Post messages to destination instance.
				PostMessages(messages, destinationInstance, destinationAuthString, uploadMessage)

				return nil
			},
		}, {
			Name:      "download",
			Usage:     "Download messages from ServiceNow",
			ArgsUsage: "[INSTANCE_NAME]",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "download-query, q",
					Usage:       "Download the messages with `ENCODED_QUERY`.",
					Destination: &downloadQuery,
				},
			},
			Action: func(context *cli.Context) error {
				return cli.NewExitError("Not yet implemented!", 2)
			},
		}, {
			Name:      "upload",
			Usage:     "Upload message to ServiceNow.",
			ArgsUsage: "[INSTANCE_NAME]",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "upload-language, l",
					Usage:       "Upload the messages, force the language to `LANG_CODE`.",
					Destination: &uploadMessage.Language,
				},
				&cli.StringFlag{
					Name:        "upload-application, a",
					Usage:       "Upload the messages, force the application to `APPLICATION_NAME`.",
					Destination: &uploadMessage.Application,
				},
			},
			Action: func(context *cli.Context) error {
				return cli.NewExitError("Not yet implemented!", 3)
			},
		},
	}
	
	err := app.Run(os.Args)
	panicOnError("App run failed", err, 4)
	os.Exit(0)
}
