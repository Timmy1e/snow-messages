/*
 * © 2018 ArchBox.xyz.
 * Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

package main

import "fmt"

type Message struct {
	Key         string `json:"key"`
	Message     string `json:"message"`
	Language    string `json:"language"`
	Application string `json:"application"`
}

func (message Message) String() string {
	return fmt.Sprintf(
		"Message{Key:\"%s\", Message:\"%s\", Language:\"%s\", Application:\"%s\"}",
		message.Key,
		message.Message,
		message.Language,
		message.Application)
}
