/*
 * © 2018 ArchBox.xyz.
 * Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

package main

import "fmt"

type SnowResponse struct {
	Result []Message `json:"result"`
}

func (snowResponse SnowResponse) String() string {
	return fmt.Sprintf(
		"SnowResponse{Result:\"%s\"}",
		snowResponse.Result)
}
